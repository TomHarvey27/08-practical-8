/*
  ==============================================================================

    SquareOscillator.h
    Created: 17 Nov 2016 1:50:52pm
    Author:  Thomas Harvey

  ==============================================================================
*/



#ifndef SQUAREOSCILLATOR_H_INCLUDED
#define SQUAREOSCILLATOR_H_INCLUDED

#include "Oscillator.h"

class SquareOscillator : public Oscillator
{
    
public:
    
    SquareOscillator();
    ~SquareOscillator();
    
    virtual float renderWaveShape (const float currentPhase) override;
    
private:
    

    
};



#endif  // SQUAREOSCILLATOR_H_INCLUDED
