/*
  ==============================================================================

    SquareOscillator.cpp
    Created: 17 Nov 2016 1:50:52pm
    Author:  Thomas Harvey

  ==============================================================================
*/

#include "SquareOscillator.h"


SquareOscillator::SquareOscillator()
{
    
    
    
}
SquareOscillator::~SquareOscillator()
{
    
    
    
}

float SquareOscillator::renderWaveShape (const float currentPhase)
{
    
    
    if (currentPhase <= M_PI)
        return 1.0;
    else
        return -1.0;
    
    
    
}