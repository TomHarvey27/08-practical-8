/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    
    oscillatorPointer = &sineWave;
    
    oscillatorPointer.get()->setSampleRate(44000);
    oscillatorPointer.get()->setAmplitude(1.0);
    oscillatorPointer.get()->setFrequency(440);
    
    
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here

}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        const float val = oscillatorPointer.get()->nextSample();
        *outL = val;
        *outR = val;

        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    sineWave.setSampleRate(device->getCurrentSampleRate());
    sineWave.setAmplitude(1.0);
    sineWave.setFrequency(440);
    
    squareWave.setSampleRate(device->getCurrentSampleRate());
    squareWave.setAmplitude(1.0);
    squareWave.setFrequency(440);
}

void Audio::audioDeviceStopped()
{

}

void Audio::changeOscillator(int oscillatorChoice)
{
    
    if (oscillatorChoice == 0)
    {
        oscillatorPointer = &sineWave;
    }
    
    else  if (oscillatorChoice == 1)
    {
        oscillatorPointer = &squareWave;
    }
    
    
}